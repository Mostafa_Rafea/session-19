import React from 'react';
import styled from 'styled-components';
import ToDo from './todo';

const List = styled.ul`
        padding-left: 5px;
    `;

const TodoList = ({ todos, status }) => {
    return (
        <List className="child-borders">
            {
                todos.map((todo) => {
                    return (
                        <ToDo text={todo.text} key={todo.id} id={todo.id} status={status} completed={todo.completed} />
                    )
                })
            }
        </List>
    )
}

export default TodoList;