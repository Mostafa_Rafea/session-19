import React from 'react';
import styled from 'styled-components';
import AddTodo from './add-todo';
import ToDoList from './to-do-list';

const Container = styled.div`
        background-color: #f1f1f1;
        margin: 0;
        min-height: 100vh;
    `;

const Paper = styled.div`
        width: 480px;
        background-color: white;
    `;

class App extends React.Component {

    state = {
        input: "",
        todos: []
    }

    handleChange = (e) => {
        let input = e.target.value;
        this.setState({ input });
    }

    add = (e) => {
        e.preventDefault();
        const newTodo = {
            id: this.state.todos.length,
            text: this.state.input,
            completed: false
        }

        let todos = this.state.todos;
        todos.push(newTodo);
        this.setState({
            todos,
            input: ""
        });
    }

    clear = () => {
        this.setState({
            input: "",
            todos: [],
        })
    }

    status = (id) => {
        let todos = this.state.todos.filter((todo) => {
            if (todo.id === id) {
                todo.completed = !todo.completed;
            }
            return true;
        })
        this.setState({ todos });
    }

    render() {
        return (
            <React.Fragment>
                <Container className="row flex-center flex-middle">
                    <Paper className="border border-primary padding-large margin-large no-responsive">
                        <h1>TODO-LIST</h1>
                        <AddTodo add={this.add} change={this.handleChange} value={this.state.input} />
                        <ToDoList todos={this.state.todos} status={this.status} />
                        <button onClick={this.clear} >Clear</button>
                    </Paper>
                </Container>
            </React.Fragment>
        )
    }
}

export default App;