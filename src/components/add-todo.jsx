import React from 'react';

const addTodo = ({ add, change, value }) => {
    return (
        <React.Fragment>
            <form className="row">
                <div className="col padding-right-small" >
                    <input type="text" onChange={change} value={value} />
                </div>
                <div className="col padding-left-small" >
                    <input type="submit" value="ADD" className="paper-btn btn-small" onClick={add} />
                </div>
            </form>
        </React.Fragment>
    )
}

export default addTodo;