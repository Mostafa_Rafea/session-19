import React from 'react';
import styled from 'styled-components';

const Li = styled.li`
        padding-left: 1em;
        cursor: pointer;
        overflow-wrap: break-word;
    `;

const ToDo = ({ text, status, id, completed }) => {

    return (
        <React.Fragment>
            <Li onClick={() => status(id)} className={"padding-small margin-small " + (completed ? "background-primary" : "shadow shadow-hover")} style={completed ? { 'textDecoration': "Line-through" } : {}}>{text}</Li>
        </React.Fragment >
    )
}

export default ToDo;